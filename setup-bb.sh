#!/bin/bash

info() {
    printf "\n" && printf "%0.s-" $(seq 1 ${#1}) && printf "\n"
    printf "$1\n"
    printf "%0.s=" $(seq 1 ${#1}) && printf "\n"
}

if [ "$(id -u -n)" != "root" ]; then
    info "Script must be executed as root. Run 'sudo ./setup-bb.sh'."
    exit 1
fi

read -e -p "Wifi SSID name: " SSID
read -e -p "Wifi password (min. 8 chars): " PASSWORD
read -e -p "Country code, in which the wifi is used (e.g. US): " COUNTRY

info "Disabling bluetooth"
connmanctl disable bluetooth

info "Updating software"
apt-get -y update && apt-get -y upgrade

info "Installing gcc"
apt-get -y install gcc

info "Installing make"
apt-get -y install make

info "Installing librobotcontrol"
apt-get -y install librobotcontrol

info "Installing git"
apt-get -y install git

info "Setting SSID"
sed -i "s;USE_PERSONAL_SSID=\"BeagleBone\";USE_PERSONAL_SSID=\"$SSID\";g" /etc/default/bb-wl18xx
sed -i "s;USE_APPENDED_SSID=yes;USE_APPENDED_SSID=no;g" /etc/default/bb-wl18xx

info "Setting AP password"
sed -i "s;USE_PERSONAL_PASSWORD=\"BeagleBone\";USE_PERSONAL_PASSWORD=\"$PASSWORD\";g" /etc/default/bb-wl18xx

info "Setting Wifi country code"
sed -i "s;#USE_PERSONAL_COUNTRY=US;USE_PERSONAL_COUNTRY=$COUNTRY;g" /etc/default/bb-wl18xx

info "Cloning git repository of RCCommander"
git clone https://gitlab.com/rccommander/rccommander.git

info "Making user debian owner of folder rccommander"
chown -R debian:debian rccommander/

info "Installing RCC_Client"
cd rccommander/RCC_Client
make runonboot

info "Setup finished. Time to reboot the beaglebone blue."

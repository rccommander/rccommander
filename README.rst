RCCommander
===========
Control an RC car or a drone over public LTE / mobile phone network or via wifi (direct or hotspot).

This project uses this `hardware and libraries <./doc/README.rst#Requirements>`__ to make an RC receiver whereat a computer or mobile phone (here pinephone) is the transmitter to control the RC device.

Schematic of communication
==========================
The communication is based on (unencrypted) UDP packets, so it does not matter, how the other device is reached in each case. For long range communication, a setup like in the following schematic is used.

.. figure:: ./doc/img/schematic.svg
   :width: 300px
   :alt: ./doc/img/schematic.svg

   *Fig. Schematic of communication over public mobile network*

A VPN (here wireguard) is used to (1) encrypt the traffic and (2) make the RC device accessible from another device connected to the internet. To avoid an uncontrollable infrastructure and also not rely on them, it is possible to connect directly via wifi to the car. The range will then be limited and the remote control is like a default RC car remote control, but because of the availability of the source code highly customizable. When using WPA for the wifi connection, the air traffic is encrypted.

.. figure:: ./doc/img/schematic_direct.svg
   :width: 300px
   :alt: ./doc/img/schematic_direct.svg

   *Fig. Schematic of direct communication*

With little configuration, the Raspberry Pi is able to act as a wifi hotspot, where a mobile phone or computer can directly connect and therefore control the RC device.

Interface and receiver
======================
todo

#!/usr/bin/env python3
# Standard libraries
import os
import io
import math
import time
from multiprocessing import Process, Pipe

# External libraries, pip install
import pygame

# External libraries in libs/
import libs.yaml as yaml

# Own libraries
from libs.IP_Connection import UdpServer, TcpServer


class XboxController:
    def __init__(self):
        try:
            self.joystick = pygame.joystick.Joystick(0)
            self.joystick.init()
        except:
            self.joystick = None

    def offset_correction(self, x):
        return round(127 * x + 127)

    def get_left_horizontal(self):
        return self.offset_correction(self.joystick.get_axis(0))

    def get_left_vertical(self):
        return self.offset_correction(-self.joystick.get_axis(1))

    def get_right_horizontal(self):
        return self.offset_correction(self.joystick.get_axis(3))

    def get_right_vertical(self):
        return self.offset_correction(-self.joystick.get_axis(4))

    def get_left_push(self):
        return round(self.joystick.get_button(9) * 254)

    def get_right_push(self):
        return round(self.joystick.get_button(10) * 254)

    def get_button_0(self):
        return round(self.joystick.get_button(0) * 254)

    def get_button_1(self):
        return round(self.joystick.get_button(1) * 254)

    def get_button_shutdown_client(self):
        return round(self.joystick.get_button(6) * 254)

    def get_button_exit(self):
        return round(self.joystick.get_button(7) * 254)

class GuiObjects:
    keyboard_buttons = []
    controller_buttons = []
    sticks = []
    stick_positions = []
    value_indicators = []
    display_outputs = []

    def clear_for_reload():
        GuiObjects.keyboard_buttons = []
        GuiObjects.controller_buttons = []
        GuiObjects.sticks = []
        GuiObjects.stick_positions = []
        GuiObjects.value_indicators = []

class GuiButton:
    def __init__(self, x, y, text, key):
        self.x = x
        self.y = y
        self.text = text
        self.key = key
        self.color = color_default
        self.OutputValue = None

    def colorize_button(self):
        if self.OutputValue:
            if self.OutputValue.val == 254:
                self.color = color_pressed
            else:
                self.color = color_default

    def draw(self):
        self.colorize_button()
        self._draw()

    def _draw(self):
        render = font_default.render(self.text, True, self.color)
        screen.blit(render, (self.x, self.y))

class GuiKeyboardButton(GuiButton):
    def __init__(self, x, y, text, key):
        super().__init__(x, y, text, key)
        GuiObjects.keyboard_buttons.append(self)
        self.is_pressed = False

    def get_button_value_object(self, ButtonOutputValue):
        self.OutputValue = ButtonOutputValue

    def key_down(self):
        self.is_pressed = True

    def key_up(self):
        self.is_pressed = False

class GuiControllerButton(GuiButton):
    def __init__(self, ControllerOutputValue, x, y, text, key=None):
        super().__init__(x, y, text, key)
        GuiObjects.controller_buttons.append(self)
        self.OutputValue = ControllerOutputValue

class GuiStick:
    def __init__(self, x, y, width, height, fullscreen, thickness_outer_line=2, thickness_inner_line=2):
        if not self.is_even(width):
            raise ValueError("width=%s must be even to be displayed symmetrically" % width)
        if not self.is_even(height):
            raise ValueError("height=%s must be even to be displayed symmetrically" % height)
        if not self.is_even(thickness_inner_line):
            raise ValueError("thickness_inner_line=%s must be even to be displayed symmetrically" % thickness_inner_line)

        GuiObjects.sticks.append(self)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.fullscreen = fullscreen
        self.thickness_outer_line = thickness_outer_line
        self.thickness_inner_line = thickness_inner_line
        self.middle_vertical = y + self.get_middle(height)
        self.middle_horizontal = x + self.get_middle(width)

    def is_even(self, x):
        return x % 2 == 0

    def get_middle(self, expansion):
        return math.floor(expansion / 2) - math.floor(self.thickness_inner_line / 2)

    def draw(self):
        if not self.fullscreen:
            self._draw()

    def _draw(self):
        # border of rectangle
        pygame.draw.rect(screen, color_default, [self.x, self.y, self.width, self.height])
        # hollow the rectangle
        pygame.draw.rect(screen, color_background, [self.x + self.thickness_outer_line,
                                                    self.y + self.thickness_outer_line,
                                                    self.width - self.thickness_outer_line * 2,
                                                    self.height - self.thickness_outer_line * 2])
        # vertical inner line
        pygame.draw.rect(screen, color_default, [self.middle_horizontal,
                                                  self.y + self.thickness_outer_line,
                                                  self.thickness_inner_line,
                                                  self.height - self.thickness_outer_line * 2])
        # horizontal inner line
        pygame.draw.rect(screen, color_default, [self.x + self.thickness_outer_line,
                                                 self.middle_vertical,
                                                 self.width - self.thickness_outer_line * 2,
                                                 self.thickness_inner_line])

class GuiStickPosition:
    def __init__(self, Stick, ValueIndicatorHor, ValueIndicatorVer, fullscreen):
        GuiObjects.stick_positions.append(self)
        self.x = Stick.x
        self.y = Stick.y
        self.width = Stick.width
        self.height = Stick.height
        self.ValueIndicatorHor = ValueIndicatorHor
        self.ValueIndicatorVer = ValueIndicatorVer
        self.fullscreen = fullscreen

    def val_to_move(self, expansion, x):
         return expansion * x // 254

    def draw(self):
        if not self.fullscreen:
            self._draw()

    def _draw(self):
        pygame.draw.circle(screen, color_default_alt,
                           [self.x + self.val_to_move(self.width, self.ValueIndicatorHor.val),
                            self.y + self.height - self.val_to_move(self.height, self.ValueIndicatorVer.val)],
                           5)

class GuiValueIndicator:
    def __init__(self, x, y, Input, fullscreen, rotate=0):
        GuiObjects.value_indicators.append(self)
        self.x = x
        self.y = y
        self.Input = Input
        self.fullscreen = fullscreen
        self.rotate = rotate

    def draw(self):
        if not self.fullscreen:
            self._draw()

    def _draw(self):
        render = font_default.render("%0.2f" % (self.Input.val/254), True, color_default_alt)
        if self.rotate != 0:
            render = pygame.transform.rotate(render, self.rotate)
        screen.blit(render, (self.x, self.y))

class GuiDisplayOutput:
    def __init__(self, x, y, x1, name, thresh=None, colors=None, unit=""):
        GuiObjects.display_outputs.append(self)
        self.x = x
        self.y = y
        self.x1 = x1
        self.name = name
        self.thresh = thresh
        self.colors = colors
        self.unit = unit

    def draw(self, val):
        if not (self.thresh and self.colors):
            self.color = color_default_alt
        elif val <= self.thresh[0]:
            self.color = self.colors[0]
        elif self.thresh[0] < val <= self.thresh[1]:
            self.color = self.colors[1]
        elif self.thresh[1] < val:
            self.color = self.colors[2]
        render_name = font_default.render(self.name, True, color_default_alt)
        render_value = font_default.render("%0.2f %s" % (val, self.unit), True, self.color)
        screen.blit(render_name, (self.x, self.y))
        screen.blit(render_value, (self.x1, self.y))

class GuiButtonClickableToggleController:
    def __init__(self, x, y, width, height, text, text1, Controller, fullscreen):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.text1 = text1
        self.Controller = Controller
        self.rect = pygame.Rect(x, y, width, height)
        self.txt_render = text
        self.color = color_default
        self.is_active = False
        self.fullscreen = fullscreen

        if self.Controller.joystick == None:
            self.disabled = True
        else:
            self.disabled = False

    def down(self):
        if not self.fullscreen and not self.disabled and self.clicked():
            self.toggle()

    def up(self):
        pass

    def clicked(self):
        if self.rect.collidepoint(event.pos):
            return True
        else:
            return False

    def toggle(self):
        if not self.is_active:
            self.is_active = True
            self.txt_render = self.text1
            self.color = color_pressed
        else:
            self.is_active = False
            self.txt_render = self.text
            self.color = color_default

    def draw(self):
        if not self.fullscreen:
            self._draw()

    def _draw(self):
        pygame.draw.rect(screen, color_button, self.rect)
        render = font_default.render(self.txt_render, True, self.color)
        screen.blit(render, (self.x+5, self.y+5))

class GuiPicture:
    def __init__(self, size, video):
        self.fullscreen = size["fullscreen"]
        self.x = 0 if self.fullscreen else video["x"]
        self.y = 0 if self.fullscreen else video["y"]
        self.w = size["w"] if self.fullscreen else video["w"]
        self.h = size["h"] if self.fullscreen else video["h"]
        self.dx = 0 if self.fullscreen else 2
        self.dy = 0 if self.fullscreen else 2
        self.frame_time = []
        self.img_fps = font_default.render("", True, color_default_alt)
        self.rotate = video["rotate_deg"]
        self.waiting()

    def calc_fps(self):
        if len(self.frame_time) <= 10:
            self.frame_time.append(time.time())
        else:
            self.frame_time.pop(0)
            self.frame_time.append(time.time())
            tme = 0
            nMax = len(self.frame_time) -1
            for i in range(nMax):
                tme += self.frame_time[i+1] - self.frame_time[i]
            self.fps = nMax/tme
            self.img_fps = font_default.render("%0.2f fps" % self.fps, True, color_default_alt)

    def waiting(self):
        self.img = font_default.render("Waiting for video...", True, color_default_alt)

    def border(self):
        pygame.draw.rect(screen, color_default_alt, [self.x, self.y, self.w + 4, self.h + 4], 2)

    def grey_area(self):
        area = pygame.Surface((self.w ,self.h), pygame.SRCALPHA)
        area.fill((0, 0, 0, 128))
        screen.blit(area, (self.x, self.h - 25))

    def load_image(self, data):
        stream = io.BytesIO()
        stream.write(data)
        stream.seek(0)

        try:
            img = pygame.image.load(stream)
            errImg = False
            self.calc_fps()
        except pygame.error:
            errImg = True

        if not errImg:
            self.img = pygame.transform.scale(img, (self.w, self.h))
            if self.rotate != 0:
                self.img = pygame.transform.rotate(self.img, self.rotate)
        else:
            self.waiting()

    def draw(self):
        screen.blit(self.img, (self.x + self.dx, self.y + self.dy))
        screen.blit(self.img_fps, (self.x + self.dx, self.y + self.dy))
        if self.fullscreen:
            self.grey_area()
        else:
            self.border()

class CalcObjects:
    input_switches = []
    button_output_values = []
    controller_output_values = []

    def clear_for_reload():
        CalcObjects.input_switches = []
        CalcObjects.button_output_values = []
        CalcObjects.controller_output_values = []

class ReceivedData:
    def __init__(self, battery_s):
        self.data = [-99]
        self.battery_s = battery_s
        self.battery_empty_cell = 3.3
        self.battery_full_cell = 4.2

    def volt2perc(self, v):
        perc = 100/self.battery_s/(self.battery_full_cell-self.battery_empty_cell) * (v - self.battery_s * self.battery_empty_cell)
        if perc > 100:
            perc = 100
        elif perc < 0:
            perc = 0
        return perc

    def update(self, data):
        self.data = [self.volt2perc(int.from_bytes(data[:2], "big")/100)]     # battery level

class InputSwitch:
    def __init__(self, Switch, Input, Input1, corr=None, delay=None):
        CalcObjects.input_switches.append(self)
        self.Switch = Switch
        self.Input = Input
        self.Input1 = Input1
        self.corr = corr
        self.delay = delay
        self.val_corr = None
        self.time_action = 0
        self.trigger_action = True
        if corr:
            self.lim_start = self.corr["limit"]["start"] * 254 // 100
            self.lim_stop = self.corr["limit"]["stop"] * 254 // 100
            if "mid" not in corr:
                self.mid = (self.lim_start + self.lim_stop) // 2
            else:
                self.mid = self.corr["mid"] * 254 // 100

    def calc_corrected_val(self):
        if not self.corr:
            self.val_corr = self.val
        else:
            if self.val <= 127:
                val_corr = (self.mid - self.lim_start) / 127 * self.val + self.lim_start
            else:
                val_corr = (self.lim_stop - self.mid) / 127 * (self.val - 127) + self.mid
            self.val_corr = round(val_corr)

    def update(self):
        if self.Switch.is_active:
            self.val = self.Input1.val
        else:
            self.val = self.Input.val

        if not self.delay:
            self.calc_corrected_val()
        else:
            if self.val_corr is None:
                self.calc_corrected_val()
            else:
                self.val_corr_old = self.val_corr
                self.calc_corrected_val()
                if self.val_corr_old != self.val_corr and self.trigger_action:
                    self.time_action = time.time() + self.delay
                    self.trigger_action = False
                if self.val_corr_old == self.val_corr:
                    self.trigger_action = True
                    self.time_action = 0
                if time.time() < self.time_action:
                    self.val_corr = self.val_corr_old


class ButtonOutputValue:
    def __init__(self, ButtonInc, ButtonDec=None, ButtonReset=None, ButtonReset1=None, spring=True, default_val=0):
        CalcObjects.button_output_values.append(self)
        self.ButtonInc = ButtonInc
        self.ButtonDec = ButtonDec
        self.ButtonReset = ButtonReset
        self.ButtonReset1 = ButtonReset1
        self.spring = spring
        self.toggle = True
        self.default_val = default_val
        self.val = default_val

    def update(self):
        # default behaviour, rise value and decrese value
        if self.ButtonDec != None:
            if self.ButtonInc.is_pressed and self.val < 254:
                self.val = self.val + 4 if self.val + 4 < 254 else 254
            if self.ButtonDec.is_pressed and self.val > 0:
                self.val = self.val - 4 if self.val - 4 > 0 else 0
            # spring reset
            if self.spring and not self.ButtonInc.is_pressed and not self.ButtonDec.is_pressed:
                if self.val > self.default_val:
                    self.val = self.val - 4 if self.val - 4 > 127 else 127
                elif self.val < self.default_val:
                    self.val = self.val + 4 if self.val + 4 < 127 else 127
        # binary button
        else:
            # automatic reset
            if self.spring:
                if self.ButtonInc.is_pressed and self.val == 0:
                    self.val = 254
                elif not self.ButtonInc.is_pressed and self.val == 254:
                    self.val = 0
            # toggle
            else:
                if not self.ButtonInc.is_pressed and not self.toggle:
                    self.toggle = True
                if self.ButtonInc.is_pressed and self.val == 0 and self.toggle:
                    self.val = 254
                    self.toggle = False
                elif self.ButtonInc.is_pressed and self.val == 254 and self.toggle:
                    self.val = 0
                    self.toggle = False

        if self.ButtonReset != None and self.ButtonReset1 != None:
            if self.ButtonReset.is_pressed and self.ButtonReset1.is_pressed:
                self.val = self.default_val

class ControllerOutputValue:
    def __init__(self, Controller, axis, toggle=False):
        CalcObjects.controller_output_values.append(self)
        self.Controller = Controller
        self.axis = axis
        self.toggle = toggle
        self._toggle = True
        self.val = 0

    def update(self):
        if self.axis == "left_vertical":
            val = self.Controller.get_left_vertical()
        elif self.axis == "left_horizontal":
            val = self.Controller.get_left_horizontal()
        elif self.axis == "right_vertical":
            val = self.Controller.get_right_vertical()
        elif self.axis == "right_horizontal":
            val = self.Controller.get_right_horizontal()
        elif self.axis == "left_push":
            val = self.Controller.get_left_push()
        elif self.axis == "right_push":
            val = self.Controller.get_right_push()
        elif self.axis == "button_0":
            val = self.Controller.get_button_0()
        elif self.axis == "button_1":
            val = self.Controller.get_button_1()
        elif self.axis == "shutdown_client":
            val = self.Controller.get_button_shutdown_client()
        elif self.axis == "exit":
            val = self.Controller.get_button_exit()

        if not self.toggle:
            self.val = val
        else:
            if val == 0 and not self._toggle:
                self._toggle = True
            if val == 254 and self.val == 0 and self._toggle:
                self.val = 254
                self._toggle = False
            elif val == 254 and self.val == 254 and self._toggle:
                self.val = 0
                self._toggle = False

    def reset_toggled(self):
        if self.toggle:
            self.val = 0


def load_buttons():
    # clear old buttons with old settings
    GuiObjects.clear_for_reload()
    CalcObjects.clear_for_reload()

    with open("configs/button_settings.yml", "r") as f:
        button_settings = yaml.safe_load(f)

    # left stick
    gui_stick_left = GuiStick(gui_toggle_controller.x + 10, gui_toggle_controller.y + 50, 100, 100, gui_cfg["size"]["fullscreen"])

    gui_button_left_vertical_reset = GuiKeyboardButton(gui_stick_left.x + gui_stick_left.width + offset_key_parallel[3],
                                                       gui_stick_left.y + gui_stick_left.height - 10,
                                                       "Q", pygame.K_q)

    gui_button_left_vertical_reset_1 = GuiKeyboardButton(gui_stick_left.x + gui_stick_left.width + offset_key_parallel[3] * 4,
                                                         gui_stick_left.y + gui_stick_left.height - 10,
                                                         "E", pygame.K_e)

    gui_button_left_up = GuiKeyboardButton(gui_stick_left.middle_horizontal - offset_key_orthogonal[0],
                                           gui_stick_left.y - offset_key_parallel[0],
                                           "W", pygame.K_w)

    gui_button_left_down = GuiKeyboardButton(gui_stick_left.middle_horizontal - offset_key_orthogonal[0],
                                             gui_stick_left.y + gui_stick_left.height + offset_key_parallel[1],
                                             "S", pygame.K_s)

    gui_button_left_left = GuiKeyboardButton(gui_stick_left.x - offset_key_parallel[2],
                                             gui_stick_left.middle_vertical - offset_key_orthogonal[1],
                                             "A", pygame.K_a)

    gui_button_left_right = GuiKeyboardButton(gui_stick_left.x + gui_stick_left.width + offset_key_parallel[3],
                                              gui_stick_left.middle_vertical - offset_key_orthogonal[1],
                                              "D", pygame.K_d)

    button_output_value_left_vertical = ButtonOutputValue(gui_button_left_up,
                                                          gui_button_left_down,
                                                          gui_button_left_vertical_reset,
                                                          gui_button_left_vertical_reset_1, False, 127)

    controller_output_value_left_vertical = ControllerOutputValue(controller, "left_vertical")

    switch_value_left_vertical = InputSwitch(gui_toggle_controller,
                                             button_output_value_left_vertical,
                                             controller_output_value_left_vertical,
                                             button_settings["left"]["vertical"])

    GuiValueIndicator(gui_stick_left.x + gui_stick_left.width + offset_key_parallel[3] - 3,
                      gui_stick_left.y,
                      switch_value_left_vertical, gui_cfg["size"]["fullscreen"], 90)

    button_output_value_left_horizontal = ButtonOutputValue(gui_button_left_right, gui_button_left_left, default_val=127)

    controller_output_value_left_horizontal = ControllerOutputValue(controller, "left_horizontal")

    switch_value_left_horizontal = InputSwitch(gui_toggle_controller,
                                               button_output_value_left_horizontal,
                                               controller_output_value_left_horizontal,
                                               button_settings["left"]["horizontal"])

    GuiValueIndicator(gui_stick_left.x,
                      gui_stick_left.y + gui_stick_left.height + offset_key_parallel[1],
                      switch_value_left_horizontal, gui_cfg["size"]["fullscreen"])

    GuiStickPosition(gui_stick_left,
                     switch_value_left_horizontal,
                     switch_value_left_vertical,
                     gui_cfg["size"]["fullscreen"])

    # right stick
    gui_stick_right = GuiStick(gui_stick_left.x + gui_stick_left.width * 1.5,
                               gui_stick_left.y, gui_stick_left.width, gui_stick_left.height, gui_cfg["size"]["fullscreen"])


    gui_button_right_up = GuiKeyboardButton(gui_stick_right.middle_horizontal - offset_key_orthogonal[0],
                                            gui_stick_right.y - offset_key_parallel[0],
                                            u"\u2191", pygame.K_UP)

    gui_button_right_down = GuiKeyboardButton(gui_stick_right.middle_horizontal - offset_key_orthogonal[0],
                                              gui_stick_right.y + gui_stick_right.height + offset_key_parallel[1],
                                              u"\u2193", pygame.K_DOWN)

    gui_button_right_left = GuiKeyboardButton(gui_stick_right.x - offset_key_parallel[2],
                                              gui_stick_right.middle_vertical - offset_key_orthogonal[1],
                                              u"\u2190", pygame.K_LEFT)

    gui_button_right_right = GuiKeyboardButton(gui_stick_right.x + gui_stick_right.width + offset_key_parallel[3],
                                               gui_stick_right.middle_vertical - offset_key_orthogonal[1],
                                               u"\u2192", pygame.K_RIGHT)

    button_output_value_right_vertical = ButtonOutputValue(gui_button_right_up,
                                                           gui_button_right_down, default_val=127)

    controller_output_value_right_vertical = ControllerOutputValue(controller, "right_vertical")

    switch_value_right_vertical = InputSwitch(gui_toggle_controller,
                                              button_output_value_right_vertical,
                                              controller_output_value_right_vertical,
                                              button_settings["right"]["vertical"])

    GuiValueIndicator(gui_stick_right.x + gui_stick_right.width + offset_key_parallel[3] - 3,
                      gui_stick_right.y,
                      switch_value_right_vertical, gui_cfg["size"]["fullscreen"], 90)

    button_output_value_right_horizontal = ButtonOutputValue(gui_button_right_right, gui_button_right_left, default_val=127)

    controller_output_value_right_horizontal = ControllerOutputValue(controller, "right_horizontal")

    switch_value_right_horizontal = InputSwitch(gui_toggle_controller,
                                                button_output_value_right_horizontal,
                                                controller_output_value_right_horizontal,
                                                button_settings["right"]["horizontal"])

    GuiValueIndicator(gui_stick_right.x,
                      gui_stick_right.y + gui_stick_right.height + offset_key_parallel[1],
                      switch_value_right_horizontal, gui_cfg["size"]["fullscreen"])

    GuiStickPosition(gui_stick_right,
                     switch_value_right_horizontal,
                     switch_value_right_vertical,
                     gui_cfg["size"]["fullscreen"])

    # left push
    gui_keyboard_button_left_push = GuiKeyboardButton(gui_cfg["aux_buttons"]["x"], gui_cfg["aux_buttons"]["y"], "R.", pygame.K_r)
    button_output_value_left_push = ButtonOutputValue(gui_keyboard_button_left_push, spring=False)
    gui_keyboard_button_left_push.get_button_value_object(button_output_value_left_push)

    controller_output_value_button_left_push = ControllerOutputValue(controller, "left_push", True)
    GuiControllerButton(controller_output_value_button_left_push, gui_keyboard_button_left_push.x,
                        gui_keyboard_button_left_push.y, "LP.")

    InputSwitch(gui_toggle_controller, button_output_value_left_push, controller_output_value_button_left_push, button_settings["left"]["push"])

    # right push
    gui_keyboard_button_right_push = GuiKeyboardButton(gui_keyboard_button_left_push.x + offset_aux, gui_keyboard_button_left_push.y, "0.", pygame.K_KP0)
    button_output_value_right_push = ButtonOutputValue(gui_keyboard_button_right_push, spring=False)
    gui_keyboard_button_right_push.get_button_value_object(button_output_value_right_push)

    controller_output_value_button_right_push = ControllerOutputValue(controller, "right_push", True)
    GuiControllerButton(controller_output_value_button_right_push, gui_keyboard_button_right_push.x,
                        gui_keyboard_button_right_push.y, "RP.")

    InputSwitch(gui_toggle_controller, button_output_value_right_push, controller_output_value_button_right_push, button_settings["right"]["push"])

    # aux button 0
    gui_keyboard_button_aux_0 = GuiKeyboardButton(gui_keyboard_button_right_push.x + offset_aux, gui_keyboard_button_right_push.y, "1", pygame.K_KP1)
    button_output_value_aux_0 = ButtonOutputValue(gui_keyboard_button_aux_0)
    gui_keyboard_button_aux_0.get_button_value_object(button_output_value_aux_0)

    controller_output_value_button_0 = ControllerOutputValue(controller, "button_0")
    GuiControllerButton(controller_output_value_button_0, gui_keyboard_button_aux_0.x,
                        gui_keyboard_button_aux_0.y, "A")

    InputSwitch(gui_toggle_controller, button_output_value_aux_0, controller_output_value_button_0, button_settings["button_aux_0"])

    # aux button 1
    gui_keyboard_button_aux_1 = GuiKeyboardButton(gui_keyboard_button_aux_0.x + offset_aux,
                                                  gui_keyboard_button_aux_0.y, "2", pygame.K_KP2)
    button_output_value_aux_1 = ButtonOutputValue(gui_keyboard_button_aux_1)
    gui_keyboard_button_aux_1.get_button_value_object(button_output_value_aux_1)

    controller_output_value_button_1 = ControllerOutputValue(controller, "button_1")
    GuiControllerButton(controller_output_value_button_1, gui_keyboard_button_aux_1.x,
                        gui_keyboard_button_aux_1.y, "B")

    InputSwitch(gui_toggle_controller, button_output_value_aux_1, controller_output_value_button_1, button_settings["button_aux_1"])

    # button shutdown client
    gui_keyboard_button_shutdown_client = GuiKeyboardButton(gui_keyboard_button_aux_1.x + offset_aux, gui_keyboard_button_aux_1.y, "F12 (shutdown client)", pygame.K_F12)
    button_output_value_shutdown_client = ButtonOutputValue(gui_keyboard_button_shutdown_client)
    gui_keyboard_button_shutdown_client.get_button_value_object(button_output_value_shutdown_client)

    controller_output_value_button_shutdown_client = ControllerOutputValue(controller, "shutdown_client")
    GuiControllerButton(controller_output_value_button_shutdown_client, gui_keyboard_button_shutdown_client.x,
                        gui_keyboard_button_shutdown_client.y, "shutdown client")

    InputSwitch(gui_toggle_controller, button_output_value_shutdown_client, controller_output_value_button_shutdown_client, delay=3)

    # controller exit button
    return ControllerOutputValue(controller, "exit")


# load configs
# ============
with open("configs/ip_settings.yml", "r") as f:
    ip_config = yaml.safe_load(f)
with open("configs/gui_settings.yml", "r") as f:
    gui_settings = yaml.safe_load(f)
    gui_cfg = gui_settings[gui_settings["view"]]


# default settings
# ================
pygame.init()
pygame.joystick.init()

if gui_cfg["size"]["mode_fullscreen"]:
    screen = pygame.display.set_mode((gui_cfg["size"]["w"], gui_cfg["size"]["h"]), pygame.FULLSCREEN)
else:
    screen = pygame.display.set_mode((gui_cfg["size"]["w"], gui_cfg["size"]["h"]))

clock = pygame.time.Clock()

pygame.display.set_caption("RCCommander - IP")
pygame.mouse.set_visible(1)

font_default = pygame.font.SysFont("DejaVuSansMono", 12)

color_default = (255, 255, 255)     # (r, g, b)
color_pressed = (255, 0, 0)         # (r, g, b)
color_default_alt = (0, 180, 230)   # (r, g, b)
color_background = (40, 40, 40)     # (r, g, b)
color_button = (70, 70, 70)         # (r, g, b)

offset_key_parallel = (15, 0, 10, 3)       # (top, bottom, left, right)
offset_key_orthogonal = (2, 6)             # (to vertical, to horizontal)
offset_aux = 35                            # space between aux buttons

controller = XboxController()


# udp connection
# ==============
# txc: TX commands, rxt: RX telemetry
txc_rxt = UdpServer(ip_config["RC_Commander"]["ip"], ip_config["RC_Commander"]["port"]["txc_rxt"],
                    ip_config["RCC_Client"]["ip"], ip_config["RCC_Client"]["port"]["rxc_txt"])
p_txc1, p_txc2 = Pipe(False)
proc_txc = Process(target=txc_rxt.send_loop, args=(p_txc1, 0.05), daemon=True)
proc_txc.start()

p_rxt1, p_rxt2 = Pipe(False)
proc_rxt = Process(target=txc_rxt.receive_loop, args=(p_rxt1, p_rxt2), daemon=True)
proc_rxt.start()

rx_pic = TcpServer(ip_config["RC_Commander"]["ip"], ip_config["RC_Commander"]["port"]["rx_pic"])
p_rx_pic1, p_rx_pic2 = Pipe(False)
proc_rx_pic = Process(target=rx_pic.receive_pic_loop, args=(p_rx_pic1, p_rx_pic2), daemon=True)
proc_rx_pic.start()

received_data = ReceivedData(battery_s=2)


# GUI objects
# ===========
gui_toggle_controller = GuiButtonClickableToggleController(gui_cfg["toggle_controller"]["x"], gui_cfg["toggle_controller"]["y"],
                                                           120, 25, "Controller: off", "Controller: on", controller, gui_cfg["size"]["fullscreen"])
if gui_cfg["size"]["fullscreen"] and not gui_toggle_controller.disabled:
    gui_toggle_controller.toggle()
elif gui_cfg["size"]["fullscreen"] and gui_toggle_controller.disabled:
    print("Fullscreen only works with a connected controller.")
    pygame.event.post(pygame.event.Event(pygame.QUIT))

video = GuiPicture(gui_cfg["size"], gui_cfg["video"])

GuiDisplayOutput(gui_cfg["battery"]["x"], gui_cfg["battery"]["y"], gui_cfg["battery"]["x"] + 65, "Battery:", thresh=[20, 70], colors=[(255, 0, 0), (255, 255, 0), (0, 255, 0)], unit="%")


# main routine
# ============
running = True
button_cfg_time = 0
while True:
    clock.tick(60)

    try:
        _button_cfg_time = int(os.path.getmtime("configs/button_settings.yml"))
    except FileNotFoundError:
        _button_cfg_time = 0
    if _button_cfg_time > button_cfg_time:
        button_cfg_time = _button_cfg_time
        controller_exit = load_buttons()

    screen.fill(color_background)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            proc_txc.kill()
            proc_rxt.kill()
            proc_rx_pic.kill()
            running = False
            break

        if not gui_toggle_controller.is_active:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                for obj in GuiObjects.keyboard_buttons:
                    if event.key == obj.key:
                        obj.key_down()
            if event.type == pygame.KEYUP:
                for obj in GuiObjects.keyboard_buttons:
                    if event.key == obj.key:
                        obj.key_up()
        else:
            if controller_exit.val == 254:
                pygame.event.post(pygame.event.Event(pygame.QUIT))

        if event.type == pygame.MOUSEBUTTONDOWN:
            gui_toggle_controller.down()

    if not running:
        break

    if p_rx_pic1.poll():
        video.load_image(p_rx_pic1.recv())
    video.draw()

    gui_toggle_controller.draw()
    for obj in GuiObjects.sticks:
        obj.draw()

    if not gui_toggle_controller.is_active:
        # reset toggled controller values to default
        for obj in CalcObjects.controller_output_values:
            obj.reset_toggled()
        for obj in GuiObjects.keyboard_buttons:
            obj.draw()
        for obj in CalcObjects.button_output_values:
            obj.update()
    else:
        # make all keys unpressed
        for obj in GuiObjects.keyboard_buttons:
            obj.key_up()
        # reset button_output_values to default values
        for obj in CalcObjects.button_output_values:
            obj.val = obj.default_val

        for obj in CalcObjects.controller_output_values:
            obj.update()
        for obj in GuiObjects.controller_buttons:
            obj.draw()

    for obj in CalcObjects.input_switches:
        obj.update()

    for obj in GuiObjects.value_indicators:
        obj.draw()
    for obj in GuiObjects.stick_positions:
        obj.draw()

    if p_rxt1.poll():
        received_data.update(p_rxt1.recv())
    for i, obj in enumerate(GuiObjects.display_outputs):
        obj.draw(received_data.data[i])

    if not p_txc1.poll():
        p_txc2.send(bytearray([obj.val_corr for obj in CalcObjects.input_switches]))
    pygame.display.flip()

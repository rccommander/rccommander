# Standard libraries
import socket
import time
import struct


class UdpServer:
    def __init__(self, host_ip, host_port, client_ip=None, client_port=None):
        self.host_ip = host_ip
        self.host_port = host_port
        self.client_ip = client_ip
        self.client_port = client_port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((host_ip, host_port))

    def time2bytes(self):
        return int(time.time() * 100).to_bytes(5, "big")

    def send(self, msg, timestamp):
        if timestamp:
            data = self.time2bytes() + msg
        else:
            data = msg
        self.socket.sendto(data, (self.client_ip, self.client_port))

    def receive(self):
        return self.socket.recvfrom(508)[0]

    def send_loop(self, p, delay):
        while True:
            if p.poll():
                _ = p.recv()
                self.send(p.recv(), True)
            time.sleep(delay)

    def receive_loop(self, p1, p2):
        while True:
            data = self.receive()
            if not p1.poll():
                p2.send(data)


class TcpServer:
    def __init__(self, host_ip, host_port, client_ip=None, client_port=None):
        self.host_ip = host_ip
        self.host_port = host_port
        self.client_ip = client_ip
        self.client_port = client_port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if client_ip == None and client_port == None:
            self.socket.bind((host_ip, host_port))
            self.socket.listen(1)

    def await_connection(self):
        self.conn, self.addr = self.socket.accept()
        self.file_r = self.conn.makefile("rb")

    def make_connection(self):
        self.socket.connect((self.client_ip, self.client_port))
        self.file_w = self.socket.makefile("wb")

    def receive_pic_loop(self, p1, p2):
        self.await_connection()
        while True:
            if self.server_is_alive(4):
                data = self.file_r.read(4)
            else:
                data = b""

            if data != b"":
                size = struct.unpack("<L", data)[0]

            if data != b"" and self.server_is_alive(size):
                data = self.file_r.read(size)
            else:
                data = b""

            if data != b"":
                if not p1.poll() and data:
                    p2.send(data)

            if data == b"":
                self.close_server()
                self.await_connection()

    def server_is_alive(self, size):
        try:
            # this will try to read bytes without blocking and also without removing them from buffer (peek only)
            data = self.sock.recv(size, socket.MSG_DONTWAIT | socket.MSG_PEEK)
            if len(data) == 0:
                return False
        except BlockingIOError:
            return True  # socket is open and reading from it would block
        except ConnectionResetError:
            return False  # socket was closed for some other reason
        except:
            return True

    def close_server(self):
        self.file_r.close()
        self.conn.close()

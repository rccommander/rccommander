#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <netdb.h>
#include <time.h>
#include <rc/time.h>
#include <rc/servo.h>
#include <rc/adc.h>


#define CLIENT_PORT "65431"

#define COMMANDER_IP "192.168.8.2"
#define COMMANDER_PORT "65431"

#define LEN_BYTES_RX 14
#define LEN_BYTES_TX 2

static uint8_t DEFAULT_PWM_VALUES[8] = {127, 127, 127, 127, 127, 127, 127, 127};


static int RUNNING;
static int FD_PWM_GENERATOR[2];
static int FD_RX_DATA[2];
static int FD_TX_DATA[2];

static void __signal_handler(__attribute__ ((unused)) int dummy)
{
	RUNNING = 0;
	return;
}

int pwm_data_to_us(uint8_t x)
{
	return (1000 * (x + 254) / 254);	// 0 means 1000us, 254 means 2000us
}

uint64_t get_recv_time(uint8_t *p)
{
	return (((uint64_t)p[0] << 32) + ((uint32_t)p[1] << 24) + ((uint32_t)p[2] << 16) + ((uint16_t)p[3] << 8) + (uint8_t)p[4]);
}

void udp_socket()
{
	int sockfd;
	struct addrinfo hints, *servinfo;
	struct addrinfo hintc, *clientinfo;
	uint8_t data[LEN_BYTES_RX];

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	memset(&hintc, 0, sizeof hintc);
	hintc.ai_family = AF_INET;
	hintc.ai_socktype = SOCK_DGRAM;

	getaddrinfo(NULL, CLIENT_PORT, &hints, &servinfo);
	getaddrinfo(COMMANDER_IP, COMMANDER_PORT, &hintc, &clientinfo);

	if ((sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol)) == -1)
	{
		perror("listener: socket");
		return;
	}

	if (bind(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) == -1)
	{
		close(sockfd);
		perror("listener: bind");
		return;
	}

	connect(sockfd, clientinfo->ai_addr, clientinfo->ai_addrlen);
	freeaddrinfo(servinfo);
	freeaddrinfo(clientinfo);

	if (fork() == 0)
	{
		uint8_t tele[LEN_BYTES_TX];
		close(FD_TX_DATA[1]);

		while (RUNNING)
		{
			read(FD_TX_DATA[0], tele, sizeof(uint8_t) * LEN_BYTES_TX);
			send(sockfd, tele, sizeof(uint8_t) * LEN_BYTES_TX, 0);
		}
	}
	else
	{
		close(FD_RX_DATA[0]);

		while (RUNNING)
		{
			recv(sockfd, data, LEN_BYTES_RX , 0);
			write(FD_RX_DATA[1], data, sizeof(uint8_t) * LEN_BYTES_RX);
		}
	}
	close(sockfd);
	return;
}

void pwm_generator()
{
	int pwm_values_us[8];  		// one int for each pwm channel
	int i;

	close(FD_PWM_GENERATOR[1]);  	// only receives from parent, so close writing end of pipe

	if (rc_servo_init()) return;

	for (i = 0; i < 8; i++)
	{
		pwm_values_us[i] =  pwm_data_to_us(DEFAULT_PWM_VALUES[i]);
	}

	while (RUNNING)
	{
		while (read(FD_PWM_GENERATOR[0], pwm_values_us, sizeof(int) * 8) != -1);
		for (i = 0; i < 8; i++)
		{
			rc_servo_send_pulse_us(i+1, pwm_values_us[i]);
		}

		rc_usleep(20000);

	}
	rc_servo_cleanup();
	close(FD_PWM_GENERATOR[0]);
	return;
}

void send_tele()
{
	uint8_t tele[LEN_BYTES_TX] = {0, 0};
	uint16_t pack_voltage;

	close(FD_TX_DATA[0]);

	if(rc_adc_init()==-1) return;

	while (RUNNING)
	{
		pack_voltage = 100 * rc_adc_batt();
		tele[0] = (uint8_t)((pack_voltage & 0xFF00) >> 8);
		tele[1] = (uint8_t)((pack_voltage & 0x00FF));
		write(FD_TX_DATA[1], tele, sizeof(uint8_t) * LEN_BYTES_TX);
		sleep(2);
	}

	rc_adc_cleanup();
	close(FD_TX_DATA[1]);
	return;
}

int main()
{
	int i;
	pid_t pid_to_kill;

	RUNNING = 1;

	signal(SIGINT, __signal_handler);

	pipe(FD_PWM_GENERATOR);
	fcntl(FD_PWM_GENERATOR[0], F_SETFL, fcntl(FD_PWM_GENERATOR[0], F_GETFL) | O_NONBLOCK);

	pipe(FD_RX_DATA);
	fcntl(FD_RX_DATA[0], F_SETFL, fcntl(FD_RX_DATA[0], F_GETFL) | O_NONBLOCK);

	pipe(FD_TX_DATA);

	for (i = 0;  i < 4; i++)
	{
		if (i==1 && fork() == 0)
		{
			pwm_generator();
			return 0;
		}
		if (i==2)
		{
			pid_to_kill = fork();
			if (pid_to_kill == 0)
			{
				udp_socket();
				return 0;
			}
		}
		if (i==3 && fork() == 0)
		{
			send_tele();
		}
	}

	close(FD_PWM_GENERATOR[0]);
	close(FD_RX_DATA[1]);

	uint8_t data_recv[LEN_BYTES_RX] = {0, 0, 0, 0, 0};
	int pwm_values_us[8];
	uint64_t c_time = 0, o_time = 0;
	time_t dc_time = 0;
	while (RUNNING)
	{
		while (read(FD_RX_DATA[0], data_recv, sizeof(uint8_t) * LEN_BYTES_RX) != -1);
		for (i = 0; i < 8; i++)
		{
			pwm_values_us[i] = pwm_data_to_us(data_recv[i+5]);
		}
		c_time = get_recv_time(data_recv);
		if (c_time > o_time)
		{
			o_time = c_time;
			dc_time = time(0);
			write(FD_PWM_GENERATOR[1], pwm_values_us, sizeof(int) * 8);
		}
		if (time(0) >= dc_time + 2)
		{
			for (i = 0; i < 8; i++)
			{
				pwm_values_us[i] = pwm_data_to_us(DEFAULT_PWM_VALUES[i]);
			}
			write(FD_PWM_GENERATOR[1], pwm_values_us, sizeof(int) * 8);
			sleep(1);
		}

		rc_usleep(10000);

	}


	close(FD_PWM_GENERATOR[1]);
	close(FD_RX_DATA[0]);
	kill(pid_to_kill, SIGKILL);
	return 0;

}
